-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `task`;
CREATE TABLE `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(256) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `due_at` date NOT NULL,
  `assigned_to` int(11) NOT NULL,
  `priority` enum('1','2','3','4') NOT NULL DEFAULT '4',
  `status` enum('open','close') NOT NULL DEFAULT 'open',
  `done_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `task` (`id`, `description`, `created_at`, `created_by`, `due_at`, `assigned_to`, `priority`, `status`, `done_by`) VALUES
(19,	'hello',	'2012-02-02 00:00:00',	3,	'2012-02-02',	2,	'2',	'close',	1);

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user` (`id`, `name`, `email`, `password`) VALUES
(1,	'root',	'root',	'root'),
(2,	'hello',	'hello',	'hello'),
(3,	'Pour',	'',	'');

-- 2017-06-14 12:35:01
