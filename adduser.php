<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
<head>
	<?php require_once('tpl/head.php'); ?>
</head>
<body>
	<div class="off-canvas-wrapper">
		<?php require_once('tpl/header.php'); ?>

		<main class="container off-canvas-content" data-off-canvas-content>
			<div class="row">
				<h1 class="page-title">Ajouter un utilisateur</h1>
				<form method="post" action="adduser-action.php" class="small-12 medium-6 collumn">
					<label>Nom</label>
					<input type="text" name="name"/>
					<label>E-mail</label>
					<input type="text" name="email"/>
					<label>Mot de passe</label>
					<input type="password" name="password"/>
					<input type="submit" value="Ajouter" class="button"/>
				</form>
			</div>
		</main>

		<?php require_once('tpl/footer.php'); ?>
	</div>
</body>
</html>
