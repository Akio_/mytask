<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
<head>
	<?php require_once('tpl/head.php'); ?>
</head>
<body>
	<div>
		<?php require_once('tpl/header.php'); ?>
		<main class="container off-canvas-content" data-off-canvas-content>
			<div>
				<iframe class="manual" src="manual.pdf"></iframe>
			</div>
			<?php require_once('tpl/footer.php'); ?>
		</main>
	</body>
	</html>