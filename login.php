<?php
require_once('inc/config.php');

if(isset($_SESSION['userid']))
	header('Location:index.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
<head>
	<?php require_once('tpl/head.php'); ?>
</head>
<body class="log">
	<div class="row columns small-12 medium-10 large-10">
		<h1 class="page-title">Login</h1>
		<form method="post" action="logme.php">
			<label for="email">E-mail</label>
			<input class="mail" type="text" name="email" id="email"/>

			<label for="password">Mot de passe</label>
			<input class="pw" type="password" name="password" id="password"/>

			<input type="submit" value="Se connecter" class="button"/>
		</form>
	</div>

	<?php require_once('inc/script.php'); ?>
</body>
</html>
