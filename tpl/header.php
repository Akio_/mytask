<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<header class="top row expanded">
	<div class="top-bar">
		<div class="top-bar-title">
			<button class="menu-icon hide-for-large" type="button" data-toggle="offCanvas"></button>
			<a href="index.php" class="title">My Task</a>
		</div>
		<div class="top-bar-right">
			<?php
			$query =	$db -> prepare('SELECT * FROM user WHERE id = ?');
			$query -> execute(array($_SESSION['userid']));
			$data = $query -> fetch();
			?>
			<ul class="dropdown menu" data-dropdown-menu>
				<li class="img">
					<img src="css/img/profil_<?php echo $_SESSION['userid']; ?>.jpg" class="header-picture" />
					<ul class="menu">
						<li><a href="edituser.php?id=<?php echo $_SESSION['userid']; ?>" class="menu-menu"><?php echo $data['name']; ?></a></li>
						<li><a href="logout.php" class="menu-menu">Logout</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</header>

<nav class="nav off-canvas-absolute position-left reveal-for-large" id="offCanvas" data-off-canvas>
	<ul class="vertical menu">
		<li><a href="add.php" class="add">Ajouter une tâche</a></li>
		<li><a href="users.php" class="user">Liste des utilisateurs</a></li>
		<li><a href="adduser.php" class="adduser">Ajouter un utilisateur</a></li>
		<li><a href="manual.php" class="view_manual">Mode d'emploi</a></li>
		<li><a href="cv.php" class="view_cv">Présentation personnelle</a></li>
		<li><a href="about.php" class="view_about">About</a></li>
	</ul>
</nav>
