<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
<head>
	<?php require_once('tpl/head.php'); ?>
</head>
<body>
	<div class="off-canvas-wrapper">
		<?php require_once('tpl/header.php');	?>

		<main class="container off-canvas-content" data-off-canvas-content>
			<div class="row">
				<h1 class="page-title">Liste des utilisateurs</h1>
				<ul class="tasklist">
					<li class="tasklist-header">
						<span class="tasklist-item-id">
							ID
						</span>
						<span class="tasklist-item-priority">
							Utilisateur
						</span>
						<span class="tasklist-item-description">
							Email
						</span>
						<span class="tasklist-item-actions">
							Actions
						</span>
					</li>
					<?php
					$query = $db -> query('SELECT * FROM user');
					while($data = $query -> fetch()):
						?>
					<li class="tasklist-item">
						<span class="tasklist-item-id">
							<?php echo $data['id']; ?>
						</span>
						<span class="tasklist-item-priority">
							<?php echo $data['name']; ?>
						</span>
						<span class="tasklist-item-description">
							<?php echo $data['email']; ?>
						</span>
						<span class="tasklist-item-actions">
							<a href="edituser.php?id=<?php echo $data['id']; ?>">
								<i class="fa fa-pencil" aria-hidden="true"></i>
							</a>
							<a href="#" data-deleteuser="<?php echo $data['id']; ?>">
								<i class="fa fa-times" aria-hidden="true"></i>
							</a>
						</span>
					</li>
				<?php endwhile; ?>
			</ul>
		</div>
	</main>

	<?php require_once('tpl/footer.php');	?>
</div>
</body>
</html>
